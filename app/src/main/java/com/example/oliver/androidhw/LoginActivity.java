package com.example.oliver.androidhw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    static final String NAME_KEY = "NAME";
    static final String EMAIL_KEY = "EMAIL";

    private Button btnLogin;
    private EditText etUserName, etEmail, etPassword;
    private String userName, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button) findViewById(R.id.btn_login);
        etUserName = (EditText) findViewById(R.id.userName);
        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private void login() {
        initialize();
        if (validate()) {
            Toast.makeText(getApplicationContext(), "Login success!!!", Toast.LENGTH_SHORT).show();
            onLoginSuccess();
        } else {
            Toast.makeText(getApplicationContext(), "Login has failed!!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void onLoginSuccess() {
        linkToUser();
    }

    private void linkToUser() {
        Intent intent = new Intent(LoginActivity.this, UserActivity.class);
        intent.putExtra(NAME_KEY, userName);
        intent.putExtra(EMAIL_KEY, email);
        startActivity(intent);
    }

    private void initialize() {
        userName = etUserName.getText().toString().trim();
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
    }

    private boolean validate() {
        return checkName() & checkEmail() & checkPassword();
    }

    private boolean checkName() {
        if (userName.isEmpty()) {
            etUserName.setError("Please enter your user name");
            return false;
        }
        if (userName.length() < 5 || userName.length() > 20) {
            etUserName.setError("Name has to have more than 5 characters");
            return false;
        }
        return true;
    }

    private boolean checkEmail() {
        if (email.isEmpty()) {
            etEmail.setError("Please enter email address");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Please enter valid email address");
            return false;
        }
        return true;
    }

    private boolean checkPassword() {
        if (password.isEmpty()) {
            etPassword.setError("Please enter password");
            return false;
        }
        if (password.length() < 5) {
            etPassword.setError("Password has to have more than 5 characters");
            return false;
        }
        return true;
    }
}
