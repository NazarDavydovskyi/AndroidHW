package com.example.oliver.androidhw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            TextView nameView = (TextView) findViewById(R.id.nameView);
            TextView emailView = (TextView) findViewById(R.id.emailView);

            String name = extras.getString(LoginActivity.NAME_KEY);
            nameView.setText(name);

            String email = extras.getString(LoginActivity.EMAIL_KEY);
            emailView.setText(email);
        }
    }
}
